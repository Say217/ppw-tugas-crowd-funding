from django.apps import AppConfig


class CrowdFundingAppConfig(AppConfig):
    name = 'crowd_funding_app'
